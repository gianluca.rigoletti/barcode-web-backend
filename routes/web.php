<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/inventari', function (Request $request) use ($app) {
    $type = $request->input('type');
    if ($type === 'list') {
        return [
            "Fornovo",
            "Bibbiano",
            "Taneto",
            "Parma"
        ];
    }
    return "Taneto";
    
});

$app->get('/articoli', function (Request $request) use ($app) {
    $operatore = $request->input('operatore', 'Operatore vuoto');
    $inventario = $request->input('inventario', 'Inventario vuoto');
    $scaffale = $request->input('scaffale', 'Scaffale vuoto');
    $articolo = $request->input('articolo', 'Articolo vuoto');
    return response()->json(['desc' => "Operatore: $operatore \n Inventario: $inventario \n Scaffale: $scaffale \n Articolo: $articolo \n"]);
});


$app->get('/insert', function(Request $request) use ($app) {
    $operatore = $request->input('operatore', 'Operatore vuoto');
    $inventario = $request->input('inventario', 'Inventario vuoto');
    $scaffale = $request->input('scaffale', 'Scaffale vuoto');
    $articolo = $request->input('articolo', 'Articolo vuoto');
    $qta = $request->input('qta', 0);
    if ($qta !== 0) {
        return response()->json(['status' => true]);
    }
    return response()->json(['status' => false]);
});